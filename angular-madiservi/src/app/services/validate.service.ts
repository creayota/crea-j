import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ValidateService {

  constructor() { }

  validateRegister(cliente){
    if(cliente.nombre == undefined || cliente.apellido == undefined || cliente.username == undefined || cliente.correo == undefined || cliente.DUI == undefined || cliente.telefono == undefined || cliente.direccion == undefined || cliente.empresa == undefined || cliente.contrasena == undefined){
      return false;
    } else {
      return true;
    }
  }

  validateEmail(email){
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  }

  validateDUI(DUI){
    const re = /^\d{8}-\d{1}$/;
    return re.test(String(DUI).toLowerCase())
  }
  validateTelefono(telefono){
    const re = /^\d{8}$/;
    return re.test(String(telefono).toLocaleLowerCase());
  }
}
