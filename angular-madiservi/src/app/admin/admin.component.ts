import { Component, OnInit } from '@angular/core';
import {ValidateService} from '../services/validate.service';
import {AuthService} from '../services/auth.service';
import {Router} from '@angular/router';
import { HttpHeaders } from '@angular/common/http';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import {NgFlashMessageService} from 'ng-flash-messages';
import { Alert } from 'selenium-webdriver';
import { Location, JsonPipe } from '@angular/common';
import { HttpClient } from '@angular/common/http'
import { from } from 'rxjs';


@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {
  username:any;
  contrasena:any;
  pic:File = null;
  adloged:any = localStorage.getItem('admin_log');
  loged:any = localStorage.getItem('loged');
  invent:boolean = true;
  categorias:any;
  categoria:any;

  nombre:string;
  cod:string;
  desc:string;
  cat:number;
  picc:any;
  exis:any;
  pedidos:any;
  clientes:any;
  i:any;

  constructor(private validateService: ValidateService,
    private AuthService: AuthService,
    private router: Router,
    private ngFlashMessageService: NgFlashMessageService) { }
  addProd(){
    this.invent = false;
  }
  onSubmit(){
    this.invent = true;
  }
  onChangePic(event){
    let reader = new FileReader();
    if(event.target.files && event.target.files.length > 0) {
      let file = event.target.files[0];
      let exp = /jpg|jpeg|png/;
      console.log(file.name.split(".").pop());
      let ext = file.name.split(".").pop();
      if(!exp.test(ext)){
        alert("Ingrese una imagen adecuada");
        this.pic = null;
        return 0;
      }
      reader.onloadend = ()=>{
        this.picc = event.target.files[0];
      }
      if(file){
        reader.readAsDataURL(file);
      }
    }
  }
  onSubmitAddCat(){
    const categoria = {
      id: "",
      nombre: this.categoria
    }
    alert("siuu");
    this.AuthService.addCategoria(categoria).subscribe(data=>{
      const msg = this.AuthService.obtainValue(data);
      const err = this.AuthService.obtainValueErr(data);
      if(msg){
        this.ngFlashMessageService.showFlashMessage({
          messages: [msg],
          dismissible: true,
          timeout: false,
          type: 'success'
        });
      }else{
        this.ngFlashMessageService.showFlashMessage({
          messages: [err],
          dismissible: true,
          timeout: false,
          type: 'danger'
        });
      }
    })
  }
  onSubmitAdd(event){
    let formData = new FormData();
    var img = "";
    formData.append("uploads[]",this.picc,this.picc.name);
    this.AuthService.upload(formData).subscribe(dataa=>{
      const producto = {
        nombre: this.nombre.toLocaleLowerCase(),
        cod: this.cod.toLocaleLowerCase(),
        desc:this.desc,
        pic:dataa,
        cat:this.cat,
        exis:'1'
      }
      this.AuthService.addProducto(producto).subscribe(data=>{
        const msg = this.AuthService.obtainValue(data);
        const err = this.AuthService.obtainValueErr(data);
        if(msg){
          this.ngFlashMessageService.showFlashMessage({
            messages: [msg],
            dismissible: true,
            timeout: false,
            type: 'success'
          });
        }else{
          this.ngFlashMessageService.showFlashMessage({
            messages: [err],
            dismissible: true,
            timeout: false,
            type: 'danger'
          });
          this.AuthService.delete(producto).subscribe(data=>{
            console.log(data);
          });
        }
      }, error =>{console.log("Error ",error);
      });
    });
    console.log(img);
    
  }
  onSubmitAd(){
    const admin ={
      username: this.username,
      contrasena: this.contrasena
    }
    this.AuthService.authAdmin(admin).subscribe(data=>{
      const msg = this.AuthService.obtainValue(data);
      if(this.AuthService.storeAdmin(data)){
        this.ngFlashMessageService.showFlashMessage({
          messages: [msg],
          dismissible: true,
          timeout: false,
          type: 'danger'
        });
      }else{
        this.ngFlashMessageService.showFlashMessage({
          messages: ['Sesion Iniciada'],
          dismissible: true,
          timeout: false,
          type: 'success'
        });
        location.reload();
      }
    },err=>{
      console.log(err);
    })
  }
  ngOnInit() {
    if(this.loged == '1'){
      this.router.navigate(['/']);
    }
    this.AuthService.callCats().subscribe(data=>{
      this.categorias = data;
    })
    this.AuthService.getPedidos().subscribe(data=>{
      this.pedidos = data;
    });
    var ped = this.pedidos;
    var i = ped.length;
    console.log(i);
  }

}
