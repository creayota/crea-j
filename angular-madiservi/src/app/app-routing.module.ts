import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { NosotrosComponent } from './nosotros/nosotros.component';
import { ProductComponent } from './product/product.component';
import { RegisterComponent } from './register/register.component';
import { LoginComponent } from './login/login.component';
import { AdminComponent } from './admin/admin.component';
import { ProductoComponent } from './producto/producto.component';
import { SoporteComponent } from './soporte/soporte.component';
import { CarComponent } from './car/car.component';
import { PedidosComponent } from './pedidos/pedidos.component';

const routes: Routes = [
  {path:'',component:HomeComponent},
  {path:'nosotros',component:NosotrosComponent},
  {path:'productos',component:ProductComponent},
  {path:'registro',component:RegisterComponent},
  {path:'login',component:LoginComponent},
  {path:'admin',component:AdminComponent},
  {path:'producto/:id',component:ProductoComponent},
  {path:'soporte',component:SoporteComponent},
  {path:'carrito',component:CarComponent},
  {path:'pedido/:id',component:PedidosComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
