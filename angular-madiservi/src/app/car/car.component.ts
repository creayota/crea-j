import { Component, OnInit, ɵConsole } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {AuthService} from '../services/auth.service';
import {NgFlashMessageService} from 'ng-flash-messages';
import { from } from 'rxjs';

@Component({
  selector: 'app-car',
  templateUrl: './car.component.html',
  styleUrls: ['./car.component.css']
})
export class CarComponent implements OnInit {
  products:Array<any>;
  cant:Array<String>;
  elim:any;
  a:any;
  loged:any = localStorage.getItem('loged');
  username:any = localStorage.getItem('username');
  constructor(private AuthService:AuthService,private _route: ActivatedRoute,private ngFlashMessageService: NgFlashMessageService) { }
  eliminar(num){
    localStorage.removeItem('producto-'+num);
    location.reload();
  }
  ngOnInit() {
    var pro = [];
    var ca = [];
    var j = 0;
    for(var i=0;i<100;i++){
      if(localStorage.getItem('producto-'+i) != null){
        let txt = localStorage.getItem('producto-'+i).split(',');
        j = i;
        this.AuthService.obtenerProducto({cod:localStorage.getItem('producto-'+i).split(',')[0]}).subscribe(data=>{
          pro.push({data:data,cant:txt[1],num:j});
        })
      }
    }
    this.a = j;
    this.products = pro;
    console.log(this.products);
  }
  send(){
    var count = this.a;
    var prod = this.products;
    if(!this.loged){
      this.ngFlashMessageService.showFlashMessage({
        messages: ["Inicia sesión para poder hacer una compra"],
        dismissible: true,
        timeout: false,
        type: 'danger'
      });
      return 0;
    }
    const carrito = {
      id:0,
      cliente:this.username
    }
    this.AuthService.sendCarrito(carrito).subscribe(data=>{
      for(var i=0;i<count;i++){
        let pedido = {
          car:data,
          pro: prod[i].data.codigo,
          cant: prod[i].cant
        }
        console.log(pedido);
        this.AuthService.sendPedido(pedido).subscribe(dat=>{
          console.log(data);
        })
        localStorage.removeItem("producto-"+(i+1));
      }
    })
    this.products = [];
    this.a = 0;
  }

}
