import { Component, OnInit, Input } from '@angular/core';
import {ValidateService} from '../services/validate.service';
import {AuthService} from '../services/auth.service';
import {Router} from '@angular/router';
import { JsonPipe } from '@angular/common';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})

export class NavbarComponent implements OnInit {
  nombre = localStorage.getItem('nombre');
  apellido = localStorage.getItem('apellido');
  loged: any = localStorage.getItem('loged');
  adloged:any = localStorage.getItem('admin_log');
  categorias:any;
  buscar:string;
  cat:string;
  productos:any;
  constructor(private AuthService:AuthService, private Router:Router) { }
  search_rt(){
    if(this.buscar == ""){
      this.productos = [];
      document.getElementById("out").style.display="none";
    } else {
      const producto = {
        nombre: this.buscar
      }
      document.getElementById("out").style.display="block";
      this.AuthService.buscarProductos(producto).subscribe(data=>{
        this.productos = data;
      })
    }
  }
  ngOnInit() {
    this.AuthService.callCats().subscribe(data=>{
      this.categorias = data;
    })
  }
  onLogout(){
    console.log("Sesion Cerrada");
    this.AuthService.logout();
    location.reload();
  }

}
