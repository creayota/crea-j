import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {AuthService} from '../services/auth.service';

@Component({
  selector: 'app-pedidos',
  templateUrl: './pedidos.component.html',
  styleUrls: ['./pedidos.component.css']
})
export class PedidosComponent implements OnInit {
productos:any;
  constructor(private AuthService:AuthService,private _route: ActivatedRoute) { }
  ngOnInit() {
    var productos = [];
    this.AuthService.getPedido({car:this._route.snapshot.paramMap.get('id')}).subscribe(dataa=>{
      for(var i = 0;i<300;i++){
        this.AuthService.obtenerProducto({cod:dataa[i].id_pro}).subscribe(data=>{
          productos.push({data:data,cant:dataa[0].cant});
        })
      }
    });
    this.productos = productos;
    console.log(productos);
  }

}
