import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { NosotrosComponent } from './nosotros/nosotros.component';
import { NavbarComponent } from './navbar/navbar.component';
import { FooterComponent } from './footer/footer.component';
import { WorkingComponent } from './working/working.component';
import { ProductComponent } from './product/product.component';
import { RegisterComponent } from './register/register.component';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { ValidateService } from './services/validate.service';
import { AuthService } from './services/auth.service';
import { LoginComponent } from './login/login.component';
import { NgFlashMessagesModule } from 'ng-flash-messages';
import { AdminComponent } from './admin/admin.component';
import { ProductoComponent } from './producto/producto.component';
import { SoporteComponent } from './soporte/soporte.component';
import { CarComponent } from './car/car.component';
import { PedidosComponent } from './pedidos/pedidos.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    NosotrosComponent,
    NavbarComponent,
    FooterComponent,
    WorkingComponent,
    ProductComponent,
    RegisterComponent,
    LoginComponent,
    AdminComponent,
    ProductoComponent,
    SoporteComponent,
    CarComponent,
    PedidosComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    NgFlashMessagesModule.forRoot()
  ],
  providers: [ValidateService,AuthService],
  bootstrap: [AppComponent]
})
export class AppModule { }
