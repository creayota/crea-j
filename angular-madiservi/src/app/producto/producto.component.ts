import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {AuthService} from '../services/auth.service';

@Component({
  selector: 'app-producto',
  templateUrl: './producto.component.html',
  styleUrls: ['./producto.component.css']
})
export class ProductoComponent implements OnInit {
  codigo:any;
  proc:any;
  cant:number = 1;
  categoria:any;
  amount:any=0;
  productos:any;
  constructor(private AuthService:AuthService,private _route: ActivatedRoute) { }
  search_cli(){
    location.reload();
  }

  upnum(){
    var cant = (<HTMLInputElement>document.getElementById("cant")).value;
    this.cant = parseInt(cant)+1;
  }
  downnum(){
    var cant = (<HTMLInputElement>document.getElementById("cant")).value;
    this.cant = parseInt(cant)-1;
    if(this.cant == 0){
      this.cant = 1;
    }
  }
  validate(){
    var cant = (<HTMLInputElement>document.getElementById("cant")).value;
    if(this.cant < 1){
      this.cant = 1;
    }
  }
  add(){
    const prod = [this.codigo,this.cant];
    var j=1;
    for(var i = 0;i<200;i++){
      if(localStorage.getItem('producto-'+i) != null){
        if(localStorage.getItem('producto-'+i).split(',')[0] == prod[0]){
          let txt = localStorage.getItem('producto-'+i).split(',');
          localStorage.removeItem('producto-'+i);
          this.AuthService.storeProd([txt[0],parseInt(txt[1])+this.cant],i);
          j=0;
          break;
        }
      }
    }
    if(j==1){
      this.AuthService.storeProd(prod,this.amount+1);
      this.amount++;
    }
    location.reload();

  }
  ngOnInit() {
    var temp = [];
    for(var i=0;i<1000;i++){
      if(localStorage.getItem('producto-'+i) != null){
        this.amount=this.amount+1;
        temp.push(localStorage.getItem('producto-'+i));
      }
    }
    this.productos = temp;
    this.codigo = this._route.snapshot.paramMap.get('id');
    const pro = {
      cod:this.codigo
    }
    this.AuthService.obtenerProducto(pro).subscribe(data=>{
      this.proc = data;
    });
  }

}
