const mongoose = require("mongoose");
const bcrypt = require("bcryptjs");
const config = require("../config/database")

//Categoria
const PedidoSchema = mongoose.Schema({
    id_car:{
        type:Number,
        require: true
    },
    id_pro:{
        type:String,
        require:true
    },
    cant:{
        type:String,
        require:true
    }
})
const Pedido = module.exports = mongoose.model("Pedido",PedidoSchema);
module.exports.getPedido = (pedido,callback)=>{
    const query = {id_car:pedido.id};
    Pedido.find(query,callback);
}
module.exports.getPedidos = (callback)=>{
    Pedido.find(callback);
}
module.exports.addPedido = function(newPedido,callback){
    newPedido.save(callback);
}