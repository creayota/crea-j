const mongoose = require("mongoose");
const bcrypt = require("bcryptjs");
const config = require("../config/database")

//Categoria
const CategoriaSchema = mongoose.Schema({
    id:{
        type:Number,
        require: true
    },
    nombre:{
        type:String,
        require:true
    }
})
const Categoria = module.exports = mongoose.model("Categoria",CategoriaSchema);
module.exports.getLastId = (callback)=>{
    Categoria.find(callback).sort({$natural:-1}).limit(1);
    if(callback == []){
        callback = {
            id:0,
            nombre:""
        }
    }
}
module.exports.getOneCat = (categor,callback)=>{
    const cat = {id:categor.id};
    Categoria.find(cat,callback);
}
module.exports.getCategorias = (callback)=>{
    Categoria.find(callback);
}
module.exports.addCat = function(newCategoria,callback){
    this.getLastId((err,callbac)=>{
        if(err) throw err;
        if(callbac == []){
            newCategoria.id = 1;
        } else {
            newCategoria.id = parseInt(callbac[0].id) + 1 ;
        }
        newCategoria.save(callback);
    });
}