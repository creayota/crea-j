const mongoose = require("mongoose");
const bcrypt = require("bcryptjs");
const config = require("../config/database")

//Categoria
const CarritoSchema = mongoose.Schema({
    id:{
        type:Number,
        require: true
    },
    cliente:{
        type:String,
        require:true
    }
})
const Carrito = module.exports = mongoose.model("Carrito",CarritoSchema);
module.exports.getLastId = (callback)=>{
    Carrito.find(callback).sort({$natural:-1}).limit(1);
    if(callback == []){
        callback = {
            id:0,
            cliente:""
        }
    }
}
module.exports.getCarritos = (callback)=>{
    Carrito.find(callback);
}
module.exports.addCarrito = function(newCarrito,callback){
    this.getLastId((err,callbac)=>{
        if(err) throw err;
        if(callbac == []){
            newCarrito.id = 1;
        } else {
            newCarrito.id = parseInt(callbac[0].id) + 1 ;
        }
        newCarrito.save(callback);
    });
}