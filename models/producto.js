const mongoose = require("mongoose");
const bcrypt = require("bcryptjs");
const config = require("../config/database")

// Client Schema

const ProductoSchema = mongoose.Schema({
    nombre: {
        type: String,
        required: true
    },
    codigo: {
        type: String,
        require: true
    },
    categoria: {
        type: String,
        require: true
    },
    descripcion: {
        type: String,
        require: true
    },
    existencia: {
        type: String,
        require: true
    },
    imagen: {
        type: String,
        require: true
    }
});
const Producto = module.exports = mongoose.model("Producto",ProductoSchema);

module.exports.getProductosByCod = (codig, callback)=>{
    const query = {codigo:{$all:codig}}
    Producto.find(query,callback);
}
module.exports.getProductoByCod = (codig, callback)=>{
    const query = {codigo:codig.codigo}
    Producto.findOne(query,callback);
}
module.exports.getProductoByNombre = (nombre, callback)=>{
    let name = new RegExp(nombre.nombre);
    const query = {nombre:name};
    Producto.find(query,callback);
}
module.exports.getProductoByCat = (cat,callback)=>{
    const query = {categoria:cat};
    Producto.find(query,callback);
}
module.exports.getProducto = (callback)=>{
    Producto.find(callback)
}
module.exports.addProducto = function(newProducto, callback){
    this.getProductoByCod(newProducto.codigo, (err,call)=>{
        if(call != null){
            callback('Codigo ya ingresado');
        } else {
            newProducto.save(callback);
        }
    })
}
