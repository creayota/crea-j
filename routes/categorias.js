const express = require("express");
const router = express.Router();
const passport = require("passport");
const jwt = require("jsonwebtoken");
const Categoria = require("../models/categoria");
const config = require("../config/database");

//Add Cat
router.post('/addcategoria',(req,res)=>{
    let newCategoria = new Categoria ({
        id: req.body.id,
        nombre: req.body.nombre
    });
    if(!newCategoria.nombre){
        return res.json({err:"Llenar todos los campos"})
    }
    Categoria.addCat(newCategoria,(err,call)=>{
        if(err){
            res.json(err);
        } else {
            res.json({msg:"Categoría registrada correctamente"});
        }
    })
});
router.post('/callone',(req,res)=>{
    let categoria = new Categoria({
        id: req.body.id.categoria,
        nombre: req.body.nombre
    })
    Categoria.getOneCat(categoria,(err,call)=>{
        if(err){
            res.json(err);
        } else {
            res.json(call);
        }
    });
});
//Call Cat
router.post('/categoria',(req,res)=>{
    Categoria.getCategorias((err,call)=>{
        if(err) throw err;
        if(!call){
            res.json({msg:"No se pudo completar"});
        } else {
            res.json(call);
        }
    });
});

module.exports = router;