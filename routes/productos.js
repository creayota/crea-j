const express = require("express");
const router = express.Router();
const passport = require("passport");
const jwt = require("jsonwebtoken");
const Producto = require("../models/producto");
const Categoria = require("../models/producto");
const config = require("../config/database");
const multipart = require("connect-multiparty");

const multiPartMiddleware = multipart({
    uploadDir:"./images"
});


//Index
router.get('/',passport.authenticate('jwt',{session:false}),(req,res)=>{
    res.json({msg:"Authorized", profile:req.user});
});

//Call Invetario
router.post('/buscar',(req,res)=>{
    Producto.getProducto((err,callback)=>{
        res.json(callback);
    })
});
router.post('/buscarp',(req,res)=>{
    let producto = new Producto({
        nombre :req.body.nombre,
        codigo : req.body.cod,
        categoria : req.body.cat,
        descripcion : req.body.desc,
        existencia : req.body.exis,
        imagen : req.body.pic
    })
    Producto.getProductoByNombre(producto, (err,callback)=>{
        if(err){
            res.json({err:"No se ha encontrado ningun producto"});
        }else {
            res.json(callback);
        }
    });
});
router.post('/obt',(req,res)=>{
    let producto = new Producto({
        nombre :req.body.nombre,
        codigo : req.body.cod,
        categoria : req.body.cat,
        descripcion : req.body.desc,
        existencia : req.body.exis,
        imagen : req.body.pic
    })
    Producto.getProductoByCod(producto, (err,callback)=>{
        if(err){
            res.json({err:"No se ha encontrado ningun producto"});
        }else {
            res.json(callback);
        }
    })
})

router.post('/obta',(req,res)=>{
    let productos = req.body.code;
    Producto.getProductosByCod(productos, (err,callback)=>{
        if(err){
            res.json({err:"No se ha encontrado ningun producto"});
        }else {
            res.json(callback);
        }
    })
})

//Add Producto
router.post('/ingresar',(req,res)=>{
    let newProducto = new Producto({
        nombre :req.body.nombre,
        codigo : req.body.cod,
        categoria : req.body.cat,
        descripcion : req.body.desc,
        existencia : req.body.exis,
        imagen : req.body.pic
    })
    Producto.getProductoByCod(newProducto, (err,callback=null)=>{
        if(callback == null){
            Producto.addProducto(newProducto, (err,callback)=>{
                if(err){
                    res.json({err:"No se ha podido ingresar el producto"});
                } else {
                    res.json({msg:"Producto Ingresado"});
                }
            })
        } else {
            res.json({err:"Código de producto ya ingresado"});
        }
    })
    
});

//Default
router.get('*',(req,res)=>{
    res.send("Invalid");
});
module.exports = router;