const express = require("express");
const router = express.Router();
const passport = require("passport");
const jwt = require("jsonwebtoken");
const Pedido = require("../models/pedido");
const config = require("../config/database");

//Add Cat
router.post('/addpedido',(req,res)=>{
    let newPedido = new Pedido ({
        id_car: req.body.car,
        id_pro: req.body.pro,
        cant:req.body.cant
    });
    if(!newPedido.id_car){
        return res.json({err:"Llenar todos los campos"})
    }
    Pedido.addPedido(newPedido,(err,call)=>{
        if(err){
            res.json(err);
        } else {
            res.json({msg:"Ingresado"});
        }
    })
});
router.post('/getpedido',(req,res)=>{
    const pedido = {
        id:req.body.car
    }
    Pedido.getPedido(pedido,(err,call)=>{
        if(err){
            res.json(err);
        } else {
            res.json(call);
        }
    });
});
router.post('/getpedidos',(req,res)=>{
    Pedido.getPedidos((err,call)=>{
        if(err){
            res.json(err);
        } else {
            res.json(call);
        }
        console.log(call);
    });
});

module.exports = router;