const express = require("express");
const router = express.Router();
const passport = require("passport");
const jwt = require("jsonwebtoken");
const Carrito = require("../models/carrito");
const config = require("../config/database");

router.post('/addcarrito',(req,res)=>{
    const carrito = new Carrito({
        cliente:req.body.cliente
    });
    console.log(carrito);
    Carrito.addCarrito(carrito,(err,call)=>{
        if(err) throw err;
        if(!call){
            res.json({msg:"No se pudo completar"});
        } else {
            Carrito.getLastId((err,calll)=>{
                res.json(calll[0].id);
            })
        }
    });
});
router.post('/getcarritos',(req,res)=>{
    Carrito.getCarritos((err,call)=>{
        if(err){
            res.json(err);
        } else {
            res.json(call);
        }
    });
});

module.exports = router;