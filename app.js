const express = require("express");
const path = require("path");
const bodyParser = require("body-parser");
const cors = require("cors");
const passport = require("passport");
const mongoose = require("mongoose");
const config = require("./config/database.js");
const formidable = require("express-formidable");
const fs = require("fs");

//Connect to database
mongoose.connect(config.uris,config.options);
mongoose.connection.on('connected', () => {
    console.log('Connected to database'+config.uris);
})
mongoose.connection.on('error', (err) => {
    console.log('Database error'+err);
})


const app = express();
app.use(express.static('images'));
const users = require('./routes/clientes');
const admin = require('./routes/admin');
const productos = require('./routes/productos');
const categorias = require('./routes/categorias');
const carrito = require('./routes/carritos');
const pedidos = require('./routes/pedidos');


//port Number
const port = 3000;

//CORS Middleware
app.use(cors());
const multipart = require('connect-multiparty');

const multipartMiddleware = multipart({  
    uploadDir: './images'
});

//Set static Folfer
app.use(express.static(path.join(__dirname, 'public')));

// Body Parser Middleware
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
  }));

app.post('/upload',multipartMiddleware,(req,res)=>{
    console.log(req.files.uploads[0].path.split("/").pop());
    res.json(
        req.files.uploads[0].path.split("/").pop()
    );
});
app.post('/delete',(req,res)=>{
    fs.unlink("./images/"+req.body.pic, (err) => {
        if (err) {
            res.json("failed to delete local image:"+err);
        } else {
            res.json('successfully deleted local image');                                
        }
    });
});

// Passport Middleware
app.use(passport.initialize());
app.use(passport.session());

require('./config/passport')(passport);

app.use('/clientes',users);
app.use('/admin',admin);
app.use('/productos',productos);
app.use('/categorias',categorias);
app.use('/carritos',carrito);
app.use('/pedidos',pedidos);


//Index Route
app.get('/', (req,res)=> {
    res.send("Invalid Endpoint");
});

//Start Server
app.listen(port, () => {
    console.log("Server started on port "+port);
});